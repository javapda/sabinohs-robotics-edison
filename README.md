# sabinohs-robotics-edison

* [Sabino High School](http://sabinohs.tusd1.schooldesk.net/) Robotics Club focus on [Edison](https://meetedison.com/)

## resources ##

* [home on bitbucket](https://bitbucket.org/javapda/sabinohs-robotics-edison)
* [EdPy](https://meetedison.com/robot-programming-software/edpy/) | [Student Worksheets [pdf]](https://meetedison.com/content/EdPy-student-worksheets-complete.pdf)
* [Edison Robots](http://edpyapp.com/) 
* [Meet Edison](https://meetedison.com/)
* [Getting Started with Edison Guide [pdf]](https://meetedison.com/content/Get-started-with-Edison-guide-English.pdf)
* [Program EdWareApp](http://edwareapp.com/)



